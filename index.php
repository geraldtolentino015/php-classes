<?php

class API
{
  public function name($name) {
    echo "Full Name: $name<br>";
  }

  public function hobbies($hobbies) {
    echo "Hobbies:<br>";
    foreach ($hobbies as $hobby) {
      echo "&emsp;$hobby<br>";
    }
  }

  public function others($others) {
    echo "Age: $others->age<br>";
    echo "Email: $others->email<br>";
    echo "Birthday: $others->birthday<br>";
  }
}

$api = new API();

$api->name("Gerald Kent V. Tolentino");
$api->hobbies(["Painting", "Playing Videogames", "Learning New Things"]);

$others = new stdClass();
$others->age = 21;
$others->email = "geraldtolentino015@gmail.com";
$others->birthday = "2002-05-15";

$api->others($others);